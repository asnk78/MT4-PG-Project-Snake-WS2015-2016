﻿18.12.15 Erstellung der Changelog /Readme / Contributing


03.01.16 upload Version v.0.1  // Tastursteuerung eingebaut und Spielfeldrand erstellt.


08.01.16 upload Version v.0.2 // Quellcode verbesserung bzw. vereinfachungen


08.01.16 upload Version v.0.3 // Hinzufügen von Struct 

08.01.16 upload Version v.0.4 // Verbesserte Tastatursteurung eingabaut und Schlange zeichnung erstellt.

08.01.16 upload Version v.0.5 // Hinzufügen der Grenzen und Gameover wenn sich die Schlange selbst frisst

09.01.16 upload Version v.0.6 // Hinzufügen der Generierung von Zufallsessen und dass Schlange länger wird wenn sie frisst

09.01.16 upload Version v.0.7-final // Neustart funktion hinzugefügt und Punktestandanzeige.