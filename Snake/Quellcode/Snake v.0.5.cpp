//TODO  Zufallsessen, Grenzen, Abruch durch sich selber beißen ,  Punktestand, Längenänderung, 

// #define NOKIAGREEN 0x92bf00 

#include "stdafx.h"
#include "time.h"
#include <conio.h>

char bew;
char bew_cache;
int zaehler = 0;
int snake_kopf_x = 7; 
int snake_kopf_y = 7;
int kopf_richtung_x = 1;
int kopf_richtung_y = 0;
int s_laenge = 4;

void Spielfeld()
{
	groesse(33,33);
	loeschen();
	formen("s");
	flaeche(NOKIAGREEN);
	farben(NOKIAGREEN);
	rahmen(BLACK);
}

struct Schlange
{
	int x[1089];
	int y[1089];
};

typedef struct Schlange snake;
snake  snk;

void Spielmenu()
{
	printf("---SNAKE---\n");

	int pruefeeingabe = 0;
	int eingabe;
	do {

		printf("1: Neues Spiel\n");
		printf("0: Beenden\n");
		printf("Eingabe: ");
		scanf_s("%d", &eingabe);
		switch (eingabe)
		{
		case 1:
			printf("Starte Spiel...");
			Spielfeld();
			pruefeeingabe = 1;
			break;

		case 0:

			printf("Ende\n");
			pruefeeingabe = 1;
			break;

		default:

			printf("Falsche Eingabe!\n");
		}


	} while (pruefeeingabe == 0);

}


void main()
{

	Spielmenu();
	do
	{
		if (zaehler == s_laenge)
			farbe2(snk.x[0], snk.y[0], NOKIAGREEN);  
			 
		farbe2(snake_kopf_x, snake_kopf_y, BLACK);  

		Sleep(100); // Schlagengeschwindkeit


		if (_kbhit())
		{

			bew = _getch();
			switch (bew)
			{
			case 'd':
				if (bew_cache != 'a' && bew_cache != 'd') 
					kopf_richtung_x = 1;
				kopf_richtung_y = 0;
				bew_cache = 'd';
				break;
			case 'w':
				if (bew_cache != 's' && bew_cache != 'w')
					kopf_richtung_y = 1;
				kopf_richtung_x = 0;
				bew_cache = 'w';
				break;
			case 'a':
				if (bew_cache != 'd' && bew_cache != 'a')
					kopf_richtung_x = -1;
				kopf_richtung_y = 0;
				bew_cache = 'a';
				break;
			case 's':
				if (bew_cache != 'w' && bew_cache != 's')
					kopf_richtung_y = -1;
				kopf_richtung_x = 0;
				bew_cache = 's';
				break;
			}

		}

		snake_kopf_x += kopf_richtung_x;
		snake_kopf_y += kopf_richtung_y;

		zaehler++;
		snk.x[zaehler] = snake_kopf_x;
		snk.y[zaehler] = snake_kopf_y;
		if (zaehler == s_laenge + 1)
		{
			for (int i = 0; i < s_laenge + 1; i++)
			{
				snk.x[i] = snk.x[i + 1];
				snk.y[i] = snk.y[i + 1];
			}
			zaehler = s_laenge;
		}
        
        if (zaehler == s_laenge && s_laenge > 3) {
            for (int j = 0; j < s_laenge; j++)
            {
                if (snake_kopf_x == snk.x[j] && snake_kopf_y == snk.y[j])
                    gameover = 1;
            }
        }
        
        if (snake_kopf_x< 0 || snake_kopf_x> 33 - 1 || snake_kopf_y< 0 || snake_kopf_y > 33 - 1)
            gameover = 1;

	}while(1);
}

