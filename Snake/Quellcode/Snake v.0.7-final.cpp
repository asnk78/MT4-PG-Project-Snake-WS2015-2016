
// #define NOKIAGREEN 0x92bf00 

#include "stdafx.h"
#include "time.h"
#include <conio.h>

char bew;
char bew_cache;
int zaehler = 0;
int snake_kopf_x = 7; 
int snake_kopf_y = 7;
int kopf_richtung_x = 1;
int kopf_richtung_y = 0;
int s_laenge = 4;
int essen_x;
int essen_y;
int essen_pruef = 0;
int gameover = 0;
int punkte = 0;

void Spielfeld()
{
	groesse(33,33);
	loeschen();
	formen("s");
	flaeche(NOKIAGREEN);
	farben(NOKIAGREEN);
	rahmen(BLACK);
}

struct Schlange
{
	int x[1089];
	int y[1089];
};

typedef struct Schlange snake;
snake  snk;

void ErzeugeEssen(int *essen_x, int *essen_y)  // Generiert zufällig essen und prüft ob das Essen auf der Schlange sich befindet
{
	srand(time(NULL));

	while (1)
	{
		for (int i = 0; i < s_laenge; i++)
		{
			if (*essen_x == snk.x[i] && *essen_y == snk.y[i])
			{
				*essen_x = rand() % 33;
				*essen_y = rand() % 33;
				essen_pruef = 1;
				break;
			}
			else
				essen_pruef = 0;

		}

		if (essen_pruef == 0)

			break;
	}

	farbe2(*essen_x, *essen_y, BLACK);
}


void ResetVar()    //  Resetet alle Variablen für den Erneuten Start des Spiels
{          
	punkte = 0;
	gameover = 0;
	zaehler = 0;
	snake_kopf_x = 7;
	snake_kopf_y = 7;
	s_laenge = 4;
	kopf_richtung_x = 1;
	kopf_richtung_y = 0;
	bew_cache;
	ErzeugeEssen(&essen_x,&essen_y);
	
}


void ResetSnakePos()   // Reset die Position der Schlange
{
	for (int i = 0; i < 33; i++)
	{
		snk.x[i] = 0;
		snk.y[i] = 0;
	}
	snk.x[0] = snake_kopf_x;
	snk.y[0] = snake_kopf_y;
}




void Spielmenu()
{
	printf("---SNAKE---\n");

	int pruefeeingabe = 0;
	int eingabe;
	do {

		printf("1: Neues Spiel\n");
		printf("0: Beenden\n");
		printf("Eingabe: ");
		scanf_s("%d", &eingabe);
		switch (eingabe)
		{
		case 1:
			printf("Starte Spiel...\n");
			Spielfeld();
			ResetVar();
			ResetSnakePos();
			pruefeeingabe = 1;
			break;

		case 0:

			printf("Ende\n");
			pruefeeingabe = 1;
			break;

		default:

			printf("Falsche Eingabe!\n");
		}


	} while (pruefeeingabe == 0);

}


void main()
{
	char newgame;

	Spielmenu();
	do
	{
		if (zaehler == s_laenge)
			farbe2(snk.x[0], snk.y[0], NOKIAGREEN); // überfärbt den letzen Block der Schlange auf Hintergrundfarbe um eine Bewegung zu erzeugen

		farbe2(snake_kopf_x, snake_kopf_y, BLACK);

		Sleep(100);  // Schlangengeschwindigkeit


		if (_kbhit())
		{

			bew = _getch();
			switch (bew)
			{
			case 'd':
				if (bew_cache != 'a' && bew_cache != 'd')
					kopf_richtung_x = 1;
				kopf_richtung_y = 0;
				bew_cache = 'd';
				break;
			case 'w':
				if (bew_cache != 's' && bew_cache != 'w')
					kopf_richtung_y = 1;
				kopf_richtung_x = 0;
				bew_cache = 'w';
				break;
			case 'a':
				if (bew_cache != 'd' && bew_cache != 'a')
					kopf_richtung_x = -1;
				kopf_richtung_y = 0;
				bew_cache = 'a';
				break;
			case 's':
				if (bew_cache != 'w' && bew_cache != 's')
					kopf_richtung_y = -1;
				kopf_richtung_x = 0;
				bew_cache = 's';
				break;
			}

		}

		snake_kopf_x += kopf_richtung_x; // Bewegt den Schlangenkopf in die vom Spieler bestimmte Kopfrichtung
		snake_kopf_y += kopf_richtung_y;

		zaehler++;
		snk.x[zaehler] = snake_kopf_x;   //Bewegt den Schlangenkörper in Kopfrichtung
		snk.y[zaehler] = snake_kopf_y;
		if (zaehler == s_laenge + 1)
		{
			for (int i = 0; i < s_laenge + 1; i++)
			{
				snk.x[i] = snk.x[i + 1];
				snk.y[i] = snk.y[i + 1];
			}
			zaehler = s_laenge;
		}

		if (zaehler == s_laenge && s_laenge > 3) {   //  Schlange beißt sich selber 
			for (int j = 0; j < s_laenge; j++)
			{
				if (snake_kopf_x == snk.x[j] && snake_kopf_y == snk.y[j])
					gameover = 1;
			}
		}

		if (snake_kopf_x < 0 || snake_kopf_x> 32 || snake_kopf_y < 0 || snake_kopf_y > 32)   Schlange bewegt sich über den Rand
			gameover = 1;

		if (essen_x == snake_kopf_x  && essen_y == snake_kopf_y)   Schlange frisst essen und Punkteanzeige 
		{
			s_laenge++;
			ErzeugeEssen(&essen_x, &essen_y);
			punkte++;
			printf("Punkte: %d\n", punkte);
		}

		if (gameover == 1)
		{
			printf("Punkte gesamt: %d\n", punkte);
			printf("Neues Spiel ? j = ja , n = nein: \n");
			newgame = getchar();
			if (newgame == 'j')
			{

				Spielfeld();
				ResetVar();
				ResetSnakePos();
			}
			if (newgame == 'n')
				break;


		}
	} while (1);
}