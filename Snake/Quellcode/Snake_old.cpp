//TODO: Tastatureingabe, Schlange + Bewegung, Spielablauf (Kontrollstrukturen), Highscore...

// #define NOKIAGREEN 0x92bf00 

#include "stdafx.h"
#include "time.h"
#include <conio.h>

void ErzeugeSpielFeld()
{
	loeschen();
	groesse(12, 12);
	formen("s");
	flaeche(NOKIAGREEN);
	for (int i = 0; i <= 143; i++)
		farbe(i, NOKIAGREEN);
}




void ErzeugeEssen()
{
	srand(time(NULL));
	farbe(rand() % 144, BLACK);

}


void ErzeugeSchlange()
{
	int laenge[] = { 0,1,2,3 };

	for (int i = 0; i <= 3; i++)
		farbe(laenge[i], BLACK);

}

void SteuerungsTEST()
{
	char taste = getchar();
	if (_kbhit())
	{
		switch (taste)
		{
		case 'w':
			printf("Du hast w gedrueckt!\n");
			break;
		case 'a':
			printf("Du hast a gedrueckt!\n");
			break;
		case 's':
			printf("Du hast s gedrueckt!\n");
			break;
		case 'd':
			printf("Du hast d gedrueckt!\n");
			break;
		}
	}

}


void main()
{

	printf("---SNAKE---\n");

	int pruefeeingabe = 0;
	int eingabe;

	do {

		printf("1: Spiel starten\n");
		printf("2: Highscore-Liste\n");
		printf("3: Test\n");
		printf("0: Beenden\n");
		printf("Eingabe: ");
		scanf_s("%d", &eingabe);
		switch (eingabe)
		{
		case 1:
			printf("Starte Spiel...");
			ErzeugeSpielFeld();
			ErzeugeEssen();
			ErzeugeSchlange();
			pruefeeingabe = 1;
			break;

		case 2: //muss noch bearbeitet  werden
			printf("Highscore\n");
			pruefeeingabe = 1;
			break;

		case 3:
			printf("TEST\n");
			SteuerungsTEST();
			break;

		case 0:

			printf("Ende\n");
			pruefeeingabe = 1;
			break;
		default:

			printf("Falsche Eingabe!\n");
		}


	} while (pruefeeingabe == 0);

}

