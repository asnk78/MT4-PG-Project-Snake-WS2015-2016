//TODO: Tastatureingabe, Schlange + Bewegung, Spielablauf (Kontrollstrukturen), Highscore...

// #define NOKIAGREEN 0x92bf00 

#include "stdafx.h"
#include "time.h"
#include <conio.h>
#include "math.h"


struct GroesseSpielfeld
{
	int x;
	int y;
};

typedef GroesseSpielfeld grspf;

int Spielfeldgroesse()
{
	int feldgroesse;
	grspf gr;
	gr.x = 22;
	gr.y = gr.x;
	feldgroesse = gr.x*gr.y;
	groesse(gr.x, gr.y);

	return feldgroesse;
}

void Spielfelddesign()
{

	loeschen();
	formen("s");
	flaeche(NOKIAGREEN);
	for (int i = 0; i <= Spielfeldgroesse(); i++)
		farbe(i, NOKIAGREEN);
}

void Grenze() //TODO  speicherung der Grenzbereiche
{
	

	for (int i = 0; i < sqrt(Spielfeldgroesse()); i++)
	{
		
		farbe(i, BLACK);
	}

	for (int i = Spielfeldgroesse() - sqrt(Spielfeldgroesse()); i < Spielfeldgroesse(); i++)
	{
		farbe(i, BLACK);
	}

	for (int i = 0; i < Spielfeldgroesse() - sqrt(Spielfeldgroesse()); i += sqrt(Spielfeldgroesse()))
	{
		farbe(i, BLACK);
	}

	for (int i = sqrt(Spielfeldgroesse())-1;i<Spielfeldgroesse();i+=sqrt(Spielfeldgroesse()))
	{
		farbe(i, BLACK);
	}


}



void Schlange()
{
	char bew;
	int poskopf=(sqrt(Spielfeldgroesse())*sqrt(Spielfeldgroesse())/2)+5;
	

	farbe(poskopf, BLACK);

	if (_kbhit)
	{
		do {
			 
			bew = _getch();
			switch (bew)
			{
			case 'd': 
				farbe(poskopf, NOKIAGREEN);
				farbe(poskopf += 1, BLACK);
				break;
			case 'w':
				farbe(poskopf, NOKIAGREEN);
				farbe(poskopf += sqrt(Spielfeldgroesse()),BLACK);
				break;
			case 'a':
				farbe(poskopf, NOKIAGREEN);
				farbe(poskopf -= 1, BLACK);
				break;
			case 's':
				farbe(poskopf, NOKIAGREEN);
				farbe(poskopf -= sqrt(Spielfeldgroesse()), BLACK);

			}
		
		} while (1);
	}
 
}


void ErzeugeEssen() //TODO spawns auf Schlange und grenze verhindern
{

	

	srand(time(NULL));
	farbe(rand() % Spielfeldgroesse(), BLACK);

}



void main()
{

	printf("---SNAKE---\n");

	int pruefeeingabe = 0;
	int eingabe;

	do {

		printf("1: Spiel starten\n");
		printf("2: Highscore-Liste\n");
		printf("0: Beenden\n");
		printf("Eingabe: ");
		scanf_s("%d", &eingabe);
		switch (eingabe)
		{
		case 1:
			printf("Starte Spiel...");
			Spielfeldgroesse();
			Spielfelddesign();
			Grenze();
			ErzeugeEssen();
			Schlange();
			pruefeeingabe = 1;
			break;

		case 2: //muss noch bearbeitet  werden
			printf("Highscore\n");
			pruefeeingabe = 1;
			break;

		case 0:

			printf("Ende\n");
			pruefeeingabe = 1;
			break;

		default:

			printf("Falsche Eingabe!\n");
		}


	} while (pruefeeingabe == 0);

}

