"Snake (englisch für Schlange) ist der Name eines Computerspielklassikers, 
bei dem man eine Schlange durch ein Spielfeld steuert und Futter 
(manchmal Apples genannt) aufnehmen soll. 
Die Schlange wird mit jedem Futterhappen länger.
Die Berührung der Spielfeldränder oder 
des eigenen Schwanzes führen zum Tod der Schlange."

Quelle: Wikipedia/Snake 

Das Ziel unseres Projektes ist das Spiel "Snake" zu programmieren. 
Um das Spiel besser visuell darstellen zu können werden wir das Programm 
Board of Symbols(BoS) verwenden.

Das Spiel Snake ist ein Geschicklichkeitsspiel, welches das Geschick eines
Spielers in Form von Punkten misst, man erhält Punkte in dem man einen 
Futterhappen isst. 

Die Schlange wird über Tastureingabe des Spielers bewegt werden,  
die entsprechenden Bewegungsanimationen müssen im BoS dargestellt werden.

Zusätzlich dazu müssen Kontrollanweisungen für den reibungslosen Ablauf des 
Spiels erstellt werden (z.B. das bei Berührung des Randes das Spiel beendet 
wird ).

Die vermutliche größte Herausfoderung wird sein die Bewegungsanimation 
entsprechend der Tastureingabe zu entwickeln. Der Lösungsansazt für dieses 
Problem wäre die Erstellung der Schlange in welchem ihre Länge als eine Array, 
im welchem die einzelnen Elemente bearbeitet werden können
(Löschen,Hinzufügen etc.).